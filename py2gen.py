#!/usr/bin/python
# -*- coding: UTF-8 -*-

import json
import sys
import os
from string import Template


class FileGenerator:
    def __init__(self, file_name, template_folder, pb_json):
        self.file_name = file_name
        self.file_name_split = file_name.split(".")
        self.pb_json = pb_json
        self.template_root = os.path.join(template_folder, 'python2')
        file = open(self.file_name_split[0] + ".py", "w", encoding="utf-8")
        file.write('#!/usr/bin/python\n')
        file.write('# -*- coding: UTF-8 -*-\n\n')
        file.write("# Machine generated code\n\n")
        file.write('import types\n')
        file.write('import rpc\n')
        file.write('import time\n')
        file.write('import traceback\n')
        file.write('import plato\n\n')
        self.gen_enum(file)
        #self.gen_stub_service(file)
        self.gen_prx(file)
        file.close()
        self.gen_struct_pb_file()
        # if 'services' in self.pb_json:
        #     for service in self.pb_json['services']:
        #         self.gen_service_api_test(service)
        
    def gen_service_api_test(self, service):
        file = open(self.file_name_split[0] + "_" + service['name'] + "_fixture.py", "w", encoding="utf-8")
        import_method_class = "import traceback\n"
        run_method = ""
        if "methods" in service:
            for method in service['methods']:
                if 'event' in method:
                    continue
                import_method_class += "import " + service['name'] + "_" + method['name'] + "_api_test\n"
                method_name = service['name']+method['name']
                run_method += '        case_'+method_name + ' = ' + service['name'] + '_' + method['name'] + '_api_test.' + method_name + '(self.prx)\n'
                run_method += '        try:\n'
                run_method += "            yield from case_" + method_name + ".run()\n"
                run_method += '        except Exception as e:\n'
                run_method += '            case_' + method_name + '.exception(traceback.format_exc())\n'
                run_method += '        case_'+method_name+'.print_result()\n'
                tfile = open(os.path.join(self.template_root, 'api_test_method.template'), 'r').read()
                method_template = Template(tfile)
                mfile = open(service['name'] + "_" + method['name'] + "_api_test.py", "w", encoding="utf-8")
                mfile.write(method_template.substitute(service_name=service['name'], method_name=method['name']))
            run_method += '        plato.get_api_test().finish("' + self.file_name_split[0] + '", True )'
        tfile = open(os.path.join(self.template_root, 'api_test.template'), 'r').read()
        server_template = Template(tfile)
        file.write(server_template.substitute(import_method_class=import_method_class, service_name=service['name'],
                                              idlname=self.file_name_split[0], run_method=run_method))

    def gen_method_ret_pb(self, file):
        if (not ("services" in self.pb_json)) or (self.pb_json["services"] is None):
            return
        if "services" in self.pb_json:
            for service in self.pb_json["services"]:
                for method in service["methods"]:
                    file.write("message " + service["name"] + "_" + method["name"] + "_ret {\n")
                    if method["retType"]["IdlType"] == "dict":
                        file.write("    " + method["retType"]["type"] + "<" + method["retType"]["key"]["type"] + "," +
                                   method["retType"]["value"]["type"] + "> " + " _1 = 1;\n")
                    elif method["retType"]["IdlType"] == "seq" or method["retType"]["IdlType"] == "set":
                        file.write(
                            "    " + method["retType"]["type"] + " " + method["retType"]["key"]["type"] + " _1 = 1;\n")
                    elif method["retType"]["IdlType"] == "void":
                        pass
                    else:
                        file.write("    " + method["retType"]["type"] + " _1 = 1;\n")
                    file.write("    ServiceBoxTraceInfo trace_info = 2;\n")
                    file.write("}\n\n")

    def gen_method_pb_file(self, file):
        if (not ("services" in self.pb_json)) or (self.pb_json["services"] is None):
            return
        if "services" in self.pb_json:
            for service in self.pb_json["services"]:
                for method in service["methods"]:
                    file.write("message " + service["name"] + "_" + method["name"] + "_args {\n")
                    n = 1
                    for arg in method["arguments"]:
                        if arg["IdlType"] == "dict":
                            file.write(
                                "    " + arg["type"] + "<" + arg["key"]["type"] + "," + arg["value"]["type"] + ">")
                        elif arg["IdlType"] == "seq" or arg["IdlType"] == "set":
                            file.write("    " + arg["type"] + " " + arg["key"]["type"])
                        elif arg["IdlType"] == "void":
                            continue
                        else:
                            file.write("    " + arg["type"])
                        file.write(" _" + str(n) + " = " + str(n) + ";\n")
                        n += 1
                    file.write("    ServiceBoxTraceInfo trace_info = " + str(n) + ";\n")
                    file.write("}\n\n")

    def gen_struct_pb_file(self):
        file = open(self.file_name_split[0] + ".service.proto", "w", encoding="utf-8")
        file.write("// Machine generated code\n\n")
        file.write('syntax = "proto3";\n\n')
        file.write("package " + self.file_name_split[0] + ";\n\n")
        file.write("message ServiceBoxTraceInfo {\n")
        file.write("    string tracer_id = 1;\n")
        file.write("    uint64 span_id = 2;\n")
        file.write("    uint64 parent_span_id = 3;\n")
        file.write("}\n\n")
        if "enums" in self.pb_json:
            for enum in self.pb_json["enums"]:
                file.write("enum " + enum["name"] + " {\n")
                file.write("    default_" + enum["name"] + " = 0;\n")
                if "fields" in enum:
                    for field in enum["fields"]:
                        file.write("    " + field["name"] + " = " + str(field["value"]) + ";\n")
                file.write("}\n\n")
        if ("structs" in self.pb_json) and (self.pb_json["structs"] is not None):
            for struct in self.pb_json["structs"]:
                file.write("message " + struct["name"] + " {\n")
                n = 1
                if "fields" in struct:
                    for field in struct["fields"]:
                        if field["IdlType"] == "dict":
                            file.write("    " + field["type"] + "<" + field["key"]["type"] + "," + field["value"][
                                "type"] + "> " + field["name"] + " = " + str(n) + ";\n")
                        elif field["IdlType"] == "seq" or field["IdlType"] == "set":
                            file.write(
                                "    " + field["type"] + " " + field["key"]["type"] + " " + field["name"] + " = " + str(
                                    n) + ";\n")
                        else:
                            file.write("    " + field["type"] + " " + field["name"] + " = " + str(n) + ";\n")
                        n += 1
                file.write("}\n\n")
        self.gen_method_pb_file(file)
        self.gen_method_ret_pb(file)
        file.close()
        os.system("protoc --cpp_out=. " + self.file_name_split[0] + ".service.proto")

    def gen_stub_service(self, file):
        if (not ("services" in self.pb_json)) or (self.pb_json["services"] is None):
            return
        for service in self.pb_json["services"]:
            method_table_str = "{}"
            if "methods" in service:
                method_table_str = '{'
                for method in service["methods"]:
                    if 'event' in method:
                        continue
                    oneway = "False"
                    if 'oneway' in method:
                        oneway = "True"
                    method_table_str += '"' + method['name'] + '":{"index":' + str(
                        method['index']) + ', "oneway":' + oneway + '},'
                method_table_str += '}'
            tfile = open(os.path.join(self.template_root, 'stub_service.template'), 'r').read()
            server_template = Template(tfile)
            file.write(server_template.substitute(service_name=service['name'], service_uuid=service['uuid'],
                                                  service_method_table=method_table_str))

    def gen_prx(self, file):
        if (not ("services" in self.pb_json)) or (self.pb_json["services"] is None):
            return
        for service in self.pb_json["services"]:
            method_str = ""
            if "methods" in service:
                for method in service["methods"]:
                    if 'event' in method:
                        continue
                    tfile = open(os.path.join(self.template_root, 'proxy_method.template'), 'r').read()
                    server_template = Template(tfile)
                    oneway_flag = '0'
                    if 'oneway' in method:
                        oneway_flag = "1"
                    has_ret = 'True'
                    if ('oneway' in method) or (method["retType"]["IdlType"] == "void"):
                        has_ret = 'False'
                    if ('oneway' in method):
                        method['timeout'] = 0
                    method_str += server_template.substitute(method_name=method['name'], service_name=service['name'],
                                                             oneway=oneway_flag, method_id=str(method['index']),
                                                             timeout=str(method['timeout']), has_retval=has_ret)
            tfile = open(os.path.join(self.template_root, 'proxy.template'), 'r').read()
            server_template = Template(tfile)
            file.write(server_template.substitute(proxy_methods=method_str, service_name=service['name'],
                                                  service_uuid=service['uuid']))

    def gen_enum(self, file):
        if "enums" in self.pb_json:
            for enum in self.pb_json["enums"]:
                file.write("class " + enum["name"] + ":\n")
                if "fields" in enum:
                    for field in enum["fields"]:
                        file.write("    " + field["name"] + " = " + str(field["value"]) + "\n")
                    file.write('\n')
                else:
                    file.write('    pass\n\n')


if __name__ == "__main__":
    file = open(sys.argv[1], "r", encoding="utf-8")
    if len(sys.argv) == 3:
        FileGenerator(sys.argv[1], sys.argv[2], json.load(file))
    else:
        FileGenerator(sys.argv[1], './template', json.load(file))
