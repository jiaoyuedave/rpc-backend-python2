#!/usr/bin/python
# -*- coding: UTF-8 -*-

#plato python2 sdk 

import types
import sys
import traceback
import imp
import glob
import time
import signal
import os
import json
import platform

# RPC方法最多支持64个参数
ARG_NUM_LIST = []
for i in range(1, 64):
    ARG_NUM_LIST.append("_" + str(i))


# Tick 对象
class TickID:
    """
    Tick对象, 代表一个执行周期

    Parameters
    ----------
    tick_id : int
        Tick ID

    gap : long
        Tick周期
    """

    def __init__(self, tick_id, gap):
        self.id = tick_id  # TICK ID
        self.gap = gap  # 执行周期, 毫秒

# Timer 对象
class Timer:
    """
    定时器
    """

    ONCE = 1  # 单次定时器
    LOOP = 2  # 循环定时器

    def __init__(self, timer_id, fn_or_gen, timeout, timer_type=ONCE):
        """
        构造

        Parameters
        ----------
            timer_id : int
                定时器ID

            fn_or_gen : func or Generator
                定时器回调

            timeout : int
                超时间隔, 毫秒

            timer_type : int
                定时器类型
        """
        self._id = timer_id
        self._fn_or_gen = fn_or_gen
        self._type = timer_type
        self._timeout = timeout
        self._cancel = False

    def cancel(self):
        """
        取消定时器
        """
        self._cancel = True

    def do_fn(self):
        """
        执行定时器回调
        """
        if self._cancel:
            return
        if self._fn_or_gen is not None:
            if not self._cancel:
                if isinstance(self._fn_or_gen, types.GeneratorType):
                    get_plato().schedule(self._fn_or_gen)
                else:
                    try:
                        self._fn_or_gen()
                    except Exception as e:
                        # 发生异常
                        get_plato().write_error_log(str(e))
                        get_plato().write_error_log(traceback.format_exc())

    def get_id(self):
        """
        获取定时器ID

        Returns
        -------
            定时器ID : long
        """
        return self._id

    def get_timeout(self):
        """
        获取Timer timeout

        Returns:
            int : Timer timeout
        """
        return self._timeout

    def is_cancel(self):
        """
        检测定时器是否取消

        Returns:
            bool
        """
        return self._cancel

    def is_loop(self):
        """
        检测是否为循环定时器

        Returns:
            bool
        """
        return self._type == self.LOOP


def create_uuid():
    return get_plato().get_rpc().CreateUUID()


class Plato:
    """
    基于RPC通信的基础框架入口类
    """

    #rpc错误码

    RPC_CALL = 1  # RPC调用请求
    RPC_RETURN = 2  # RPC调用返回
    RPC_PROXY_CALL = 3  # 通过网关转发的RPC调用请求
    RPC_PROXY_RETURN = 4  # 发送给网关的RPC调用返回
    NO_RPC = 5  # 非RPC协议
    RPC_SUB = 6  # 订阅
    RPC_PUB = 7  # 发布
    RPC_CANCEL = 8  # 取消订阅

    OK = 1  # 成功
    NOT_FOUND = 2  # 未找到服务
    EXCEPTION = 3  # 对端发生异常
    TIMEOUT = 4  # 超时
    LIMIT = 5  # 限流

    #定时器频率设置

    TICK_REALTIME = 1  # 实时
    TICK_VERY_HIGH = 10  # 非常高
    TICK_HIGH = 50  # 高
    TICK_NORMAL = 66  # 普通
    TICK_LOW = 100  # 低
    TICK_VERY_LOW = 200  # 非常低

    # 日志等级

    VERBOSE = 1  # verbose
    INFORMATION = 2  # information
    DEBUG = 3  # debug
    WARNING = 4  # warning
    EXCEPT = 5  # exception
    FAILURE = 6  # failure
    FATAL = 7  # fatal

    def __init__(self):
        """
        构造
        """
        self._running = True  # 退出标志
        self._prx_call_manager = ProxyCallManager()  # 代理调用管理器
        self._stub_manager = StubManager()  # 本地服务类管理器
        self._proxy_manager = ProxyManager()  # 代理管理器
        self._time_id = 1  # 定时器ID
        self._timer_manager = {}  # 定时管理器
        self._dyn_imports = []  # 动态导入的proxy/stub的名字
        self._tick_slots = {}  # 多周期tick, 每个slot对应一个周期
        self._init_tick_slots()  # 初始化多级定时器
        self._tick_id = 1  # tick ID
        self._log_cb = None  # External logger callback
        self._rpc = None  # RPC instance
        self._log = None  # 日志模块
        self._app_name = "plato"  # 日志添加器名称
        self._log_cfg = 'console://;line=[%y%m%d-%H:%M:%S:%U];flush=true'  # 日志配置
        self._log_inited = False  # 日志是否初始化
        self._config = None   # 配置模块

    def _init_tick_slots(self):
        """
        初始化框架的的核心tick, 有多个tick槽位, 每个槽位代表一个tick周期, 所有tick周期组成了一帧的调用, tick数量
        需要谨慎控制数量.
        """
        # 当前时间戳
        cur_milli = self._cur_milli()
        #self._tick_slots[Plato.TICK_REALTIME] = [[self._scheduler.do_schedule, cur_milli, 0]]
        self._tick_slots[Plato.TICK_VERY_HIGH] = [[self._rpc_tick, cur_milli, 0], [self._stub_manager.tick, cur_milli, 0]]
        self._tick_slots[Plato.TICK_VERY_LOW] = [[self._proxy_manager.tick, cur_milli, 0]]

    def Init(self, config_path="config.default.cfg", log_cb=None, proto_config_path='rpc_config', is_debug=False, log_cfg=None, app_name=None):
        """
        初始化框架

        Parameters
        ----------
        config_path : str, optional
            框架配置文件
        log_cb : func, optional
            日志回调, 输出日志到外部日志系统
        proto_config_path : str, optional
            服务协议相关配置
        is_debug : bool
            是否是debug模式
        log_cfg : str
            日志配置
        app_name: str
            模块名

        Returns
        -------
            Plato instance
        """
        # 参数检测
        if not os.path.exists(config_path):
            raise (RuntimeError(config_path + " not found"))
        if not os.path.exists(proto_config_path):
            raise (RuntimeError(proto_config_path + " not found"))
        # 信号处理函数
        signal.signal(signal.SIGINT, self._signal_handler)
        signal.signal(signal.SIGTERM, self._signal_handler)
        # 初始化RPC框架
        if is_debug and platform.system() == "Linux":
            import rpc_d
            import log_d
            import config_d
            self._rpc = rpc_d
            self._log = log_d
            self._config = config_d
        else:
            import rpc
            import log
            import config
            self._rpc = rpc
            self._log = log
            self._config = config
        if not self._config.OpenConfig(config_path):
            return None
        rpc_log_cb = log_cb
        if log_cb is None:
            log_cb = Plato.def_log_cb
        if log_cfg is not None:
            self._log_cfg = log_cfg
        else:
            log_cfg = self._config.GetConfig('logger_config_line')
            if self._log_cfg is not None:
                self._log_cfg = log_cfg

        if app_name is not None:
            self._app_name = app_name
        if rpc_log_cb is None:
            rpc_log_cb = Plato.def_rpc_log_cb
        if not self._rpc.Init(config_path, rpc_log_cb):
            return None
        self._log_cb = log_cb  # 设置日志回调
        # 导入proxy/stub
        self._import_rpc_stuff(proto_config_path)
        # 建立Tracer
        # self._tracer = None
        # if self._rpc.IsOpenTrace():
        #     self._tracer = Tracer()
        return self

    def get_rpc(self):
        """
        获取rpc实例
        Returns:
            rpc
        """
        return self._rpc

    def get_log(self):
        """
        获取log实例
        Returns:
            log
        """
        if not self._log_inited:
            res = self._log.NewAppender(self._app_name, self._log_cfg)
            if res is None:
                return None
            if not res:
                return None
            self._log_inited = True
        return self._log

    def get_app_name(self):
        """
        获取应用名称
        Returns:
            应用名称
        """
        return self._app_name

    def get_config(self, attr_name):
        """
        获取配置属性值

        Parameters
        ----------
            attr_name : str
                配置属性名

        Returns
        -------
            str
        """
        return self._config.GetConfig(attr_name)

    @staticmethod
    def def_log_cb(level, content):
        get_plato().get_log().Write(get_plato().get_app_name(), level, content)

    @staticmethod
    def def_rpc_log_cb(content):
        get_plato().get_log().Write(get_plato().get_app_name(), Plato.WARNING, content)

    def Deinit(self):
        """
        删除所有动态模块属性
        """
        for dyn_name in self._dyn_imports:
            delattr(self, dyn_name)
        self._rpc.Deinit()

    def run_forever(self):
        """
        运行主循环, 阻塞线程, 调用stop或发送SIGINT, SIGTERM退出
        """
        while self._running:
            self.tick()
            time.sleep(0.001)
        self.write_warn_log("Exit")
        self.Deinit()

    # def get_tracer(self):
    #     return self._tracer

    def is_open_trace(self):
        # return self._tracer is not None
        return False

    @staticmethod
    def Connect(hosts):
        """
        SDK方法
        连接到指定的服务, hosts内有多个地址时，将随机一个地址

        Parameters
        ----------
            hosts : list or tuple
                服务地址列表

        Returns
        -------
            bool
        """
        return get_plato().get_rpc().Connect(hosts)

    def stop(self):
        """
        设置退出标志
        """
        self._running = False
        self._stub_manager.stop()

    def tick_every(self, usr_gap, func_or_g):
        """
        设置一个每usr_gap毫秒执行一次func函数的定时器

        Parameters
        ----------
            usr_gap : float or int
                执行周期

            func_or_g : func
                定时器回调函数或迭代器

        Returns
        -------
            TickID tick ID, 定时器ID
        """
        if isinstance(func_or_g, types.GeneratorType):
            raise (RuntimeError("Only allows Function"))
        if isinstance(usr_gap, float):
            gap = int(usr_gap * 1000)
        else:
            gap = usr_gap
        if gap not in self._tick_slots:
            self._tick_slots[gap] = []
        tick_id = self._tick_id
        self._tick_slots[gap].append([func_or_g, self._cur_milli(), tick_id])
        self._tick_id += 1
        new_tick = TickID(tick_id, gap)
        return new_tick

    def cancel_tick(self, tick_id):
        """
        取消一个定时器

        Parameters
        ----------
            tick_id : TickID
                定时器ID
        """
        if tick_id.gap not in self._tick_slots:
            return
        slot_index = 0
        slots = self._tick_slots[tick_id.gap]
        for slot in slots:
            if slot[2] == tick_id.id:
                del slots[slot_index]
            slot_index += 1

    def tick(self):
        """
        运行一次主循环, 在程序主循环内每帧调用一次
        """
        now = self._cur_milli()
        for freq in list(self._tick_slots.keys()):
            for slot in self._tick_slots[freq]:
                if now - slot[1] > freq:
                    try:
                        slot[0]()
                    except Exception as e:
                        get_plato().write_error_log(str(e))
                        get_plato().write_error_log(traceback.format_exc())
                    slot[1] = now
                else:
                    # 第一个没有超时, 后续不需要遍历
                    break

    def new_timer(self, fn, timeout):
        """
        启动一个定时器, 到期后销毁

        Parameters:
            fn : func
                定时器回调函数

            timeout : int
                间隔，毫秒

        Returns:
            Timer
        """
        timer_id = self._new_timer(timeout)
        timer = Timer(timer_id, fn, timeout)
        # 记录定时器
        self._timer_manager[timer_id] = timer
        return timer

    def new_timer_loop(self, fn, timeout):
        """
        启动一个循环定时器, 可以调用Timer.cancel取消定时器

        Parameters:
            fn : func
                定时器回调函数

            timeout : int
                间隔，毫秒

        Returns:
            Timer
        """
        timer_id = self._new_timer(timeout)
        timer = Timer(timer_id, fn, timeout, Timer.LOOP)
        # 记录定时器
        self._timer_manager[timer_id] = timer
        return timer

    @staticmethod
    def obj_to_dict(obj):
        """
        将对象转换为字典, 如果不是字典则返回对象本身

        Args:
            obj: 对象

        Returns:
            dict : 字典 or obj
        """
        if isinstance(obj, (bool, int, float, str, dict, set, list, tuple)):
            return obj
        else:
            return vars(obj)

    @staticmethod
    def dict_to_obj(obj):
        """
        将字典转换为对象

        Args:
            obj: 字典

        Returns:
            object : object or obj
        """

        class _DictObj:
            def __init__(self, in_dict):
                for key, val in in_dict.items():
                    if isinstance(val, (list, tuple)):
                        setattr(self, key, [_DictObj(x) if isinstance(x, dict) else x for x in val])
                    else:
                        setattr(self, key, _DictObj(val) if isinstance(val, dict) else val)

        if isinstance(obj, dict):
            return _DictObj(obj)
        else:
            return obj

    def prx_call_manager(self):
        """
        获取代理调用管理器

        Returns:
            ProxyCallManager : 代理调用管理器
        """
        return self._prx_call_manager

    def stub_manager(self):
        """
        获取服务桩管理器

        Returns:
            StubManager : 服务桩管理器
        """
        return self._stub_manager

    def proxy_manager(self):
        """
        获取代理管理器

        Returns:
            ProxyManager
        """
        return self._proxy_manager

    def write_error_log(self, log):
        """
        写入错误日志

        Args:
            log: str
                日志内容
        """
        if self._log_cb is not None:
            if self._log_inited:
                self._log_cb(Plato.FAILURE, log)
            else:
                self._log_cb("["+self._app_name+"][error]" + log)
        else:
            print("["+self._app_name+"][error]" + log)

    def write_warn_log(self, log):
        """
        写入警告日志

        Args:
            log: str
                日志内容
        """
        if self._log_cb is not None:
            if self._log_inited:
                self._log_cb(Plato.WARNING, log)
            else:
                self._log_cb("["+self._app_name+"][warn]" + log)
        else:
            print("["+self._app_name+"][warn]" + log)

    def write_debug_log(self, log):
        """
        写入警告日志

        Args:
            log: str
                日志内容
        """
        if self._log_cb is not None:
            if self._log_inited:
                self._log_cb(Plato.WARNING, log)
            else:
                self._log_cb("["+self._app_name+"][debug]" + log)
        else:
            print("["+self._app_name+"][debug]" + log)

    def write_info_log(self, log):
        """
        写入信息日志

        Args:
            log: str
                日志内容
        """
        if self._log_cb is not None:
            if self._log_inited:
                self._log_cb(Plato.INFORMATION, log)
            else:
                self._log_cb("["+self._app_name+"][info]" + log)
        else:
            print("["+self._app_name+"][info]" + log)

    def write_verb_log(self, log):
        """
        写入信息日志

        Args:
            log: str
                日志内容
        """
        if self._log_cb is not None:
            if self._log_inited:
                self._log_cb(Plato.VERBOSE, log)
            else:
                self._log_cb("["+self._app_name+"][verb]" + log)
        else:
            print("["+self._app_name+"][verb]" + log)

    def _new_timer(self, timeout):
        """
        建立一个定时器

        Args:
            timeout: float or int
                定时器间隔
        Returns:
            int : 定时器ID
        """
        timer_id = self._time_id
        if not self._rpc.NewTimer(timeout, self._time_id):
            return None
        self._time_id += 1
        return timer_id

    @staticmethod
    def _cur_milli():
        """
        获取当前毫秒时间戳

        Returns:
            long : 前毫秒时间戳
        """
        return round(time.time() * 1000)

    def _import_rpc_stuff(self, rpc_config):
        """
        导入生成的proxy和stub模块, 目录内不能非proxy/stub模块代码
        """
        for file in glob.glob(os.path.join(rpc_config, "*.py")):
            idl_name = os.path.splitext(os.path.basename(file))[0]
            if idl_name != "__init__":
                # spec = importlib.util.spec_from_file_location("." + idl_name, file)
                # mod = importlib.util.module_from_spec(spec)
                # spec.loader.exec_module(mod)
                mod = imp.load_source("."+idl_name, file)
                setattr(self, idl_name, mod)
                self._dyn_imports.append(idl_name)

    def _signal_handler(self, signum, frame):
        """
        设置退出标志为False
        """
        self.stop()

    def _rpc_tick(self):
        """
        处理RPC协议对象
        """
        while True:
            evt = self._rpc.Pop()
            if evt is None:
                # 没有事件
                break
            msg_id = evt['MSGID']
            if msg_id == Plato.RPC_RETURN:
                # RPC RETURN
                self._rpc_return(evt)
            elif msg_id == Plato.RPC_PROXY_RETURN:
                # RPC PROXY RETURN
                self._rpc_return(evt)
            elif msg_id == Plato.RPC_CALL:
                # CALL STUB METHOD
                self._stub_manager.call(evt)
            elif msg_id == Plato.RPC_PROXY_CALL:
                # CALL STUB METHOD
                self._stub_manager.call(evt)
            elif msg_id == Plato.NO_RPC:
                self._no_rpc(evt)
            elif msg_id == Plato.RPC_SUB:
                self._stub_manager.on_sub(evt)
            elif msg_id == Plato.RPC_PUB:
                self._proxy_manager.on_pub(evt)
            elif msg_id == Plato.RPC_CANCEL:
                self._stub_manager.on_cancel(evt)

    def _no_rpc(self, evt):
        """
        处理非RPC调用事件

        Args:
            evt : dict
                非RPC调用事件
        """
        if evt["ERRORID"] == Plato.TIMEOUT:
            timer_id = evt["USRDATA"]
            if timer_id in self._timer_manager:
                exec_or_co = self._timer_manager[timer_id]
                del self._timer_manager[timer_id]
                if exec_or_co is not None:
                    if isinstance(exec_or_co, Timer):
                        # 执行定时器回调函数
                        exec_or_co.do_fn()
                        if exec_or_co.is_loop() and (not exec_or_co.is_cancel()):
                            # 循环定时器，重启
                            self._rpc.NewTimer(exec_or_co.get_timeout(), timer_id)
                            self._timer_manager[timer_id] = exec_or_co
                        if not exec_or_co.is_loop() or exec_or_co.is_cancel():
                            del self._timer_manager[timer_id]
                    else:
                        self.write_warn_log("unsupport rpc event")

    def _rpc_return(self, evt):
        """
        RPC调用返回协议

        Args:
            evt : dict
                协议事件
        Returns:

        """
        call_id = evt['CALLID']
        call = self._prx_call_manager.get_call(call_id)
        if call is None:
            self.write_error_log("call not found")
            return
        # if self.is_open_trace() and evt['ERRORID'] == Plato.OK:
        #     if '_1' in evt:
        #         self._tracer.TraceProxyCallReturnArrived(call_id, evt['CHANNELID'], evt['_1'])
        #     else:
        #         self._tracer.TraceProxyCallReturnArrived(call_id, evt['CHANNELID'], None)
        service_id = evt['SERVICEID']
        # 设置返回的服务ID
        call.proxy.service_id = service_id
        # 结果已经返回, 删除调用
        self._prx_call_manager.del_call(call_id)
        if call is None:
            self.write_error_log("call not found")
            # 调用未找到
            return

        if call.type == ProxyCall.EXEC_CB:
            # 回调方式调用, 异步模式
            self._rpc_return_cb(evt, call)
        else:
            # 未知类型
            raise (RuntimeError())

    @staticmethod
    def _rpc_return_cb(evt, call):
        """
        需要调用回调函数的RPC调用返回

        Args:
            evt: dict
                协议事件
            call: ProxyCall
                代理调用对象
        """
        error_id = evt['ERRORID']
        call.proxy.RecordResult(error_id)
        if error_id != Plato.OK:
            # 发生错误, 没有返回值
            call.callback(error_id, None)
        else:
            # 成功
            if '_1' in evt:
                # 有返回值, 调用
                call.callback(Plato.OK, evt['_1'])
            else:
                # 没有返回值, 调用
                call.callback(Plato.OK, None)

# 全局Plato实例
_g_plato = None


def get_plato():
    """
    获取单件Plato实例

    Returns:
        Plato单件实例
    """
    global _g_plato
    if _g_plato is None:
        _g_plato = Plato()
    return _g_plato


class ProxyCall:
    """
    代理调用
    """
    EXEC_CO = 1  # 协程(TODO)
    EXEC_CB = 2  # 回调

    def __init__(self, co_or_fn, proxy, call_type):
        """
            构造
        Args:
            co_or_fn: Coroutine or callable
                协程或调用方法
            proxy: object
                代理实例
            call_type: int
                调用类型, EXEC_CO or EXEC_CB
        """
        self.proxy = proxy
        self.callback = co_or_fn
        self.type = call_type


class ProxyCallManager:
    """
    本地发起的远程的调用管理器
    """

    def __init__(self):
        """
        构造
        """
        self._call_table = {}
        self._call_id = 0

    def add_call(self, exec_co, proxy):
        """
        添加协程调用

        Parameters:
            exec_co : Coroutine
                协程

            proxy : object
                代理实例

        Returns:
            调用ID
        """
        # 新建一个调用ID
        self._call_id += 1
        # 记录调用协程
        self._call_table[self._call_id] = ProxyCall(exec_co, proxy, ProxyCall.EXEC_CO)
        # 返回协程ID
        return self._call_id

    def add_call_fn(self, exec_fn, proxy):
        """
        添加异步方法调用

        Parameters:
            exec_fn : callable
                方法

            proxy : object
                代理实例

        Returns:
            调用ID
        """
        # 新建一个调用ID
        self._call_id += 1
        # 记录异步调用
        self._call_table[self._call_id] = ProxyCall(exec_fn, proxy, ProxyCall.EXEC_CB)
        # 返回协程ID
        return self._call_id

    def create_call_id(self):
        """
        建立一个新的调用ID

        Returns:
            int : 调用ID
        """
        self._call_id += 1
        return self._call_id

    def del_call(self, call_id):
        """
        销毁调用实例

        Args:
            call_id: 调用ID
        """
        del self._call_table[call_id]

    def get_call(self, call_id):
        """
        获取调用实例

        Args:
            call_id: int
                调用ID

        Returns:
            ProxyCall : 调用实例
        """
        if call_id not in self._call_table:
            return None
        return self._call_table[call_id]


class ProxyManager:
    """
    Proxy管理器
    """

    def __init__(self):
        """
        构造
        """
        self._proxy_dict = {}
        self._id = 1

    def add_proxy(self, prx):
        """
        添加代理实例

        Parameter:
            prx : object
                代理实例

        Returns:
            int : 代理实例ID
        """
        new_id = self._id
        self._id += 1
        self._proxy_dict[new_id] = prx
        return new_id

    def close_proxy(self, proxy_id):
        """
        关闭代理实例

        Parameters:
            proxy_id : int
                代理实例ID
        """
        if proxy_id in self._proxy_dict:
            del self._proxy_dict[proxy_id]

    def tick(self):
        """
        运行一次代理主循环
        """
        for k, v in list(self._proxy_dict.items()):
            v.tick()

    def on_pub(self, obj):
        """
        处理订阅
        Args:
            obj: 订阅初始数据

        Returns:

        """
        if 'SUBID' not in obj or 'PROXYID' not in obj:
            return
        sub_id = obj['SUBID']
        proxy_id = obj['PROXYID']
        if proxy_id not in self._proxy_dict:
            return
        self._proxy_dict[proxy_id].on_notify(sub_id, obj)


class StubCall:
    """
    本地调用信息
    """

    def __init__(self, channel_id, global_index, call_id):
        """
        构造

        Parameters:
            channel_id : int
                服务管道ID

            global_index : long
                外部客户端(通过网关)唯一标识

            call_id : int
                调用ID
        """
        self.channel_id = channel_id
        self.global_index = global_index
        self.call_id = call_id
        self.trace_info = None


class StubManager:
    """
    用户本地服务实现类管理器
    """

    def __init__(self):
        """
        构造
        """
        self._instances = {}  # 实现类实例表
        self._inst_id = 1  # 当前最新的实例ID, 用于产生新的实例ID
        self._methods = {}  # 用户实现的服务方法
        self._balance = {}  # 本地实现类负载均衡信息
        self._sub_insts = {}  # 订阅表

    def stop(self):
        """
        停止
        Returns:
        """
        for k, v in list(self._instances.items()):
            for id_and_inst in v:
                if hasattr(id_and_inst[1], 'OnBeforeDestroy'):
                    id_and_inst[1].OnBeforeDestroy()

    def register_instance(self, uuid, inst):
        """
        注册一个类实例, 外部调用注册一个类实例

        Parameters:
            uuid : long
                服务UUID

            inst : object
                服务实例

        Returns:
            int : 服务实例ID
        """
        inst_id = self._create_instance_id()
        if uuid not in self._instances:
            self._instances[uuid] = []
            self._balance[uuid] = 0
        self._instances[uuid].append((inst_id, inst))
        return inst_id

    def register_stub_method(self, uuid, method_id, f):
        """
        注册用户实现的服务方法

        Parameters:
            uuid : long
                服务UUID

            method_id : int
                方法ID

            f : callable
                服务方法
        """
        if uuid not in self._methods:
            self._methods[uuid] = {}
        self._methods[uuid][method_id] = f

    def tick(self):
        """
        运行一次Stub主循环
        """
        for k, v in list(self._instances.items()):
            for id_and_inst in v:
                id_and_inst[1].tick()

    def on_cancel(self, obj):
        """
        取消事件订阅

        Parameters:

        obj : dict
            协议对象

        Returns:
            true成功, false失败 : bool

        References:
            https://gitee.com/dennis-kk/service-box/blob/v0.3.0-alpha/src/util/python/README.md
        """
        if 'SUBID' not in obj:
            return False
        sub_id = obj['SUBID']
        if sub_id not in self._sub_insts:
            return False
        self._sub_insts[sub_id].on_cancel(sub_id)
        return True

    def on_sub(self, obj):
        """
        事件订阅

        Parameters:

        obj : dict
            协议对象

        Returns:
            true成功, false失败 : bool

        References:
            https://gitee.com/dennis-kk/service-box/blob/v0.3.0-alpha/src/util/python/README.md
        """
        if 'UUID' not in obj or 'SERVICEID' not in obj or 'SUBID' not in obj:
            get_plato().write_error_log('Invalid proto, UUID, SERVICEID, SUBID')
            return False
        if obj['UUID'] not in self._instances:
            get_plato().write_error_log('Service not found '+str(obj['UUID']))
            return False
        sub_id = obj['SUBID']
        if sub_id in self._sub_insts:
            get_plato().write_error_log('Sub ID already existed ' + sub_id)
            return False
        uuid = obj['UUID']
        inst_id = obj['SERVICEID']
        if inst_id == 0:
            # 没有指定服务实例，选取一个实例执行
            inst_id, inst = self._get_instance_balance(uuid, self._instances[uuid])
        else:
            # 指定了服务实例，查找这个实例
            inst = self._find_instance(self._instances[uuid], inst_id)
        if inst is None:
            # 实例未注册
            get_plato().write_error_log('Service instance not found ' + str(obj['UUID']))
            return False
        if 'DATA' in obj:
            inst.on_sub(obj['CHANNELID'], obj['PROXYID'], obj['SUBID'], obj['EVENT'], obj['DATA'])
            self._sub_insts[sub_id] = inst
        else:
            if '_1' in obj:
                inst.on_sub(obj['CHANNELID'], obj['PROXYID'], obj['SUBID'], obj['EVENT'], obj['_1'])
                self._sub_insts[sub_id] = inst
        return True

    def call(self, obj):
        """
        调用用户实现的本地服务类方法

        Parameters:

        obj : dict
            协议字典对象

        Returns:
            true成功, false失败 : bool

        References:
            https://gitee.com/dennis-kk/service-box/blob/v0.3.0-alpha/src/util/python/README.md
        """
        if 'UUID' not in obj or 'SERVICEID' not in obj:
            return False
        if obj['UUID'] not in self._instances:
            return False
        uuid = obj['UUID']
        inst_id = obj['SERVICEID']
        if inst_id == 0:
            # 没有指定服务实例，选取一个实例执行
            inst_id, inst = self._get_instance_balance(uuid, self._instances[uuid])
        else:
            # 指定了服务实例，查找这个实例
            inst = self._find_instance(self._instances[uuid], inst_id)
        if inst is None:
            # 实例未注册
            return False
        global_index = 0
        if 'GLOBALINDEX' in obj:
            # 有GLOBALINDEX属性，说明是通过网关转发过来的
            global_index = obj['GLOBALINDEX']
        # 获取调用方法
        method = self._get_stub_method(uuid, obj['METHODID'])
        if method is None:
            # 方法未找到
            return False
        # 构造调用参数列表
        stub_call = StubCall(obj['CHANNELID'], global_index, obj['CALLID'])
        arg_list = []
        for arg in ARG_NUM_LIST:
            if arg in obj:
                arg_list.append(obj[arg])
        # if get_plato().is_open_trace():
        #     if 'trace_info' in obj:
        #         stub_call.trace_info = obj['trace_info']
        try:
            # 构造
            g_or_ret = inst.__call__(method, stub_call, *arg_list)
            # if isinstance(g_or_ret, types.GeneratorType):
            #     # 构造一个协程, 如果不是generator则说明执行完成
            #     get_plato().scheduler().schedule(Coroutine(g_or_ret))
        except Exception as e:
            get_plato().write_error_log(str(e))
            get_plato().write_error_log(traceback.format_exc())
            return False
        return True

    def _create_instance_id(self):
        """
        建立并返回一个新的实例ID

        Returns:
            int : 新建立的实例ID
        """
        new_id = self._inst_id
        self._inst_id += 1
        return new_id

    def _get_instance_balance(self, uuid, inst_list):
        """
        以负载均衡的方式获取一个类实例, 暂实现为轮训

        Parameters:
            uuid : int
                服务UUID

            inst_list : list
                服务实例列表

        Returns:
            int, object : 服务实例ID, 服务实例
        """
        index = self._balance[uuid]
        info = inst_list[index % len(inst_list)]
        self._balance[uuid] = index + 1
        return info[0], info[1]

    @staticmethod
    def _find_instance(inst_list, inst_id):
        """
        获取服务实例

        Parameters:
            inst_list : list
                服务实例列表

            inst_id : int
                服务实例ID

        Returns:
            object : 服务实例
        """
        for item in inst_list:
            if inst_id == item[0]:
                return item[1]
        return None

    def _get_stub_method(self, uuid, method_id):
        """
        获取服务方法

        Parameters:
            uuid : long
                服务UUID

            method_id : int
                方法ID

        Returns:
            方法 : callable
        """
        if uuid not in self._methods:
            return None
        if method_id not in self._methods[uuid]:
            return None
        return self._methods[uuid][method_id]